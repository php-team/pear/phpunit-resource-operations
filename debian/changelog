phpunit-resource-operations (3.0.4-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Add Security Policy
  * Prepare release

  [ David Prévot ]
  * Set upstream metadata fields: Security-Contact
  * Update standards version to 4.6.2
  * Ship upstream security notice

 -- David Prévot <taffit@debian.org>  Sat, 16 Mar 2024 08:15:11 +0100

phpunit-resource-operations (3.0.3-4) unstable; urgency=medium

  * Install /u/s/pkg-php-tools/{autoloaders,overrides} files

 -- David Prévot <taffit@debian.org>  Sat, 09 Jul 2022 20:51:52 +0200

phpunit-resource-operations (3.0.3-3) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Wed, 06 Jul 2022 19:23:48 +0200

phpunit-resource-operations (3.0.3-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 15:20:30 -0400

phpunit-resource-operations (3.0.3-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 14:10:23 -0400

phpunit-resource-operations (3.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Ignore tests etc. from archive exports
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325
  * Prepare release

  [ David Prévot ]
  * Document gbp import-ref usage
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2020 11:06:17 -1000

phpunit-resource-operations (3.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Drop support for PHP 7.1 and PHP 7.2
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    set fields Bug-Database, Bug-Submit, Repository, Repository-Browse
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0
  * debian/rules, debian/tests/control: Adapt phpunit call for tests

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 19:07:13 -1000

phpunit-resource-operations (2.0.1-2) unstable; urgency=medium

  * Compatibility with recent PHPUnit (8)
  * Bump debhelper from old 11 to 12.
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Sat, 24 Aug 2019 17:55:01 -1000

phpunit-resource-operations (2.0.1-1) unstable; urgency=medium

  [ Remi Collet ]
  * add a minimal test

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Use debhelper-compat 11
  * Drop get-orig-source target
  * Update copyright (years)
  * Run tests during build
  * Add CI
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.2.1

 -- David Prévot <taffit@debian.org>  Fri, 05 Oct 2018 21:25:13 -1000

phpunit-resource-operations (1.0.0-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7
  * Rebuild with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Fri, 04 Mar 2016 12:35:31 -0400

phpunit-resource-operations (1.0.0-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Mon, 26 Oct 2015 12:59:50 -0400
